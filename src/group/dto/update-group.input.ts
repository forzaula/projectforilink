import {  IsNotEmpty } from 'class-validator';
import {Field, InputType} from "@nestjs/graphql";


@InputType()
export class UpdateGroupInput{
    @IsNotEmpty()
    @Field()
    readonly groupName:string
}
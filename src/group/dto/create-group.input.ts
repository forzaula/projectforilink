import {  IsNotEmpty } from 'class-validator';
import {Field, InputType} from "@nestjs/graphql";

@InputType()
export class CreateGroupInput{
    @IsNotEmpty()
    @Field()
    readonly groupName:string

}
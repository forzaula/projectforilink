import {HttpException, HttpStatus, Injectable} from "@nestjs/common";
import {InjectRepository} from "@nestjs/typeorm";
import {DeleteResult, Repository} from "typeorm";
import {GroupEntity} from "./group.entity";
import {CreateGroupInput} from "./dto/create-group.input";
import {UpdateGroupInput} from "./dto/update-group.input";



@Injectable()
export class GroupService {
    constructor(
        @InjectRepository(GroupEntity)
        private readonly groupRepository: Repository<GroupEntity>) {
    }


    async createGroup(CreateGroupInput: CreateGroupInput): Promise<GroupEntity> {
        const groupName = await this.groupRepository.findOne({groupName: CreateGroupInput.groupName})
        if (groupName) {
            throw new HttpException("This name is taken", HttpStatus.UNPROCESSABLE_ENTITY)
        }
        const newGroup = new GroupEntity()
        Object.assign(newGroup, CreateGroupInput)
        return await this.groupRepository.save({...newGroup})
    }
    async getAll(){
        return await this.groupRepository.find()
    }
    async findByID(id:number):Promise<GroupEntity>{
        return await this.groupRepository.findOne(id)
    }
    async updateGroup(id:number,UpdateGroupInput:UpdateGroupInput){
        const group= await this.findByID(id)
        if(!group){
            throw new HttpException('group does not exist',HttpStatus.NOT_FOUND)
        }
        Object.assign(group,UpdateGroupInput)
        return await this.groupRepository.save({...group})
    }
    async remove(id:number):Promise<DeleteResult>{
        const group= await this.findByID(id)
        if(!group){
            throw new HttpException('group does not exist',HttpStatus.NOT_FOUND)
        }
        return this.groupRepository.delete({id})
    }
}
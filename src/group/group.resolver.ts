import {GroupService} from "./group.service";
import {GroupEntity} from "./group.entity";
import {Args, Mutation, Query, Resolver} from "@nestjs/graphql";
import {UsePipes, ValidationPipe} from "@nestjs/common";
import {CreateGroupInput} from "./dto/create-group.input";
import {UpdateGroupInput} from "./dto/update-group.input";


@Resolver(() => GroupEntity)
export class GroupResolver {
    constructor(private readonly groupService: GroupService) {}

    @Mutation(() => GroupEntity)
    @UsePipes(new ValidationPipe())
    createUser(@Args('CreateGroupInput') CreateGroupInput: CreateGroupInput) {
        return this.groupService.createGroup(CreateGroupInput)
    }
    @Mutation(() => GroupEntity)
    @UsePipes(new ValidationPipe())
    updateGroup(@Args('UpdateGroupInput') UpdateGroupInput: UpdateGroupInput,id:number) {
        return this.groupService.updateGroup(id,UpdateGroupInput)
    }
    @Query(() => [GroupEntity], { name: 'Groups' })
    getAll() {
        return this.groupService.getAll();
    }
    @Query(() => GroupEntity, { name: 'Group' })
    findByID(id:number) {
        return this.groupService.findByID(id);
    }
    @Mutation(() => GroupEntity)
    remove(@Args("id") id: number) {
        return this.groupService.remove(id);
    }
}


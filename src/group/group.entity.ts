import { Column, Entity,ManyToMany, PrimaryGeneratedColumn} from "typeorm";
import {UserEntity} from "../user/user.entity";
import {Field, ObjectType} from "@nestjs/graphql";

@Entity({name:'groups'})
@ObjectType()
export class GroupEntity{
    @PrimaryGeneratedColumn()
    @Field()
    id:number

    @Column()
    @Field()
    groupName:string

    // @ManyToMany(()=>UserEntity,user=>user.groups)
    // @Field(type =>[UserEntity], {nullable: true})
    // users:UserEntity[]
}
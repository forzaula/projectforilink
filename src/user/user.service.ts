import {HttpException, HttpStatus, Injectable} from "@nestjs/common";
import {InjectRepository} from "@nestjs/typeorm";
import {UserEntity} from "./user.entity";
import {DeleteResult, Repository} from "typeorm";
import {CreateUserInput} from "./dto/create-user.input";
import {UpdateUserInput} from "./dto/update-user.input";



@Injectable()
export class UserService {
    constructor(
        @InjectRepository(UserEntity)
        private readonly userRepository: Repository<UserEntity>) {
    }


    async createUser(CreateUserInput: CreateUserInput): Promise<UserEntity> {
        const userByEmail = await this.userRepository.findOne({email: CreateUserInput.email})
        const userByUsername = await this.userRepository.findOne({username: CreateUserInput.username})
        if (userByEmail || userByUsername) {
            throw new HttpException("Email and username are taken", HttpStatus.UNPROCESSABLE_ENTITY)
        }
        const newUser = new UserEntity()
        Object.assign(newUser, CreateUserInput)
        return await this.userRepository.save(newUser)
    }
    async getAll(){
        return await this.userRepository.find()
    }
    async findByID(id:number):Promise<UserEntity>{
        return await this.userRepository.findOne(id)
    }
    async updateUser(id:number,UpdateUserInput:UpdateUserInput){
        const user= await this.findByID(id)
        if(!user){
            throw new HttpException('user does not exist',HttpStatus.NOT_FOUND)
        }
        Object.assign(user,UpdateUserInput)
        return await this.userRepository.save(user)
    }
    async remove(id:number):Promise<DeleteResult>{
        const user= await this.findByID(id)
        if(!user){
            throw new HttpException('user does not exist',HttpStatus.NOT_FOUND)
        }
        return this.userRepository.delete({id})
    }
}
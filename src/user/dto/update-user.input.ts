import { IsEmail, IsNotEmpty } from 'class-validator';
import {Field, InputType} from "@nestjs/graphql";

@InputType()
export class UpdateUserInput{

    @IsNotEmpty()
    @Field()
    readonly username:string

    @IsNotEmpty()
    @IsEmail()
    @Field()
    readonly email:string

    @IsNotEmpty()
    @Field()
    readonly password:string

}
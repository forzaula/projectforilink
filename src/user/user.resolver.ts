import {UserService} from "./user.service";
import {Args, Mutation, Query, Resolver} from "@nestjs/graphql";
import {UserEntity} from "./user.entity";
import { CreateUserInput} from "./dto/create-user.input";
import {UpdateUserInput} from "./dto/update-user.input";
import { UsePipes, ValidationPipe} from "@nestjs/common";

@Resolver(() => UserEntity)
export class UserResolver {
    constructor(private readonly userService: UserService) {}

    @Mutation(() => UserEntity)
    @UsePipes(new ValidationPipe())
    createUser(@Args('createUserInput') createUserInput: CreateUserInput) {
        return this.userService.createUser(createUserInput)
    }
    @Mutation(() => UserEntity)
    @UsePipes(new ValidationPipe())
    updateUser(@Args('updateUserInput') UpdateUserInput: UpdateUserInput,id:number) {
        return this.userService.updateUser(id,UpdateUserInput)
    }
    @Query(() => [UserEntity], { name: 'Users' })
    getAll() {
        return this.userService.getAll();
    }
    @Query(() => [UserEntity], { name: 'User' })
    findByID(id:number) {
        return this.userService.findByID(id);
    }
    @Mutation(() => UserEntity)
    remove(@Args("id") id: number) {
        return this.userService.remove(id);
    }
}
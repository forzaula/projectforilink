import {BeforeInsert, Column, Entity, JoinTable, ManyToMany, PrimaryGeneratedColumn} from "typeorm";
import {GroupEntity} from "../group/group.entity";
import {hash}from 'bcrypt'
import {Field, ObjectType} from "@nestjs/graphql";


@Entity({name:'users'})
@ObjectType()
export class UserEntity{
    @PrimaryGeneratedColumn()
    @Field()
    id:number

    @Column()
    @Field()
    username:string

    @Column()
    @Field()
    email:string

    @Column()
    @Field()
    password:string

    @BeforeInsert()
    async hashPassword(){
        this.password= await hash(this.password,10)
    }

    @Column('simple-array',{nullable: true})
    @Field(type => [String],{nullable:true})
    friends:string[]

    // @ManyToMany(()=>GroupEntity,group=>group.users,{eager:true})
    // @JoinTable()
    // @Field(type =>[GroupEntity], {nullable: true})
    // groups:GroupEntity[]
}
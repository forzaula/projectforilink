import { Module } from '@nestjs/common';
import {TypeOrmModule} from "@nestjs/typeorm";
import {GraphQLModule} from "@nestjs/graphql";
import config from "./ormConfig";
import {UserModule} from "./user/user.module";
import {GroupModule} from "./group/group.module";
import { join} from 'path';

@Module({
  imports: [
    GraphQLModule.forRoot({
      autoSchemaFile: join(process.cwd(), 'src/schema.gql'),
      sortSchema: true
    }),
      TypeOrmModule.forRoot(config),
    UserModule,
    GroupModule],
  controllers: [],
  providers: [],
})
export class AppModule {}
